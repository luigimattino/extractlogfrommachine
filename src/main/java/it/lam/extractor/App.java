package it.lam.extractor;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Main App to run
 *
 */
public class App {

    /*
    * Define there the config file as shown in the example config.json 
    * file present in the config-example folder
    */
    private static final String CTEMPCONFIGJSON = "c:\\temp\\config.json";
    private static final String FTEXT = "ftext";
    private static final String STIME = "stime";
    private static final String ETIME = "etime";
    private static final String CONNECTION = "connection";
    private static final String RESULT_NAME = "fname";
    private static final String RESULT_DIR = "fdir";
    private static final String LOG_NAME = "logname";
    private static final String LOG_DIR = "logdir";
    private static final String C_LOCAL = "local";
    private static final String C_SFTP = "sftp";
    private static final String SFTP_IP = "ip";
    private static final String SFTP_PORT = "port";
    private static final String SFTP_USER = "user";
    private static final String SFTP_PASSWORD = "password";
    // if the config.json file is used this field ther in unused
    private static final String FILE_NAME = "wr.log"; // "equinox.log";
    // if the config.json file is used this field ther in unused
    private static final String FILE_DIR = "C:\\WR5_NEW\\Equinox\\logs\\";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss.SSS");

    private static boolean takeIt = false;
    private static boolean considerIt = false;
    private static boolean isTextSearch = false;
    private static int textMatch = 0;
    
    private static StringBuilder textSearchMatching = new StringBuilder();
    
    public static void main(String[] args) {
        JSONObject config = getConfigJsonObject();
        String text = (String) config.get(FTEXT);
        if (text != null && !"".equals(text.trim())) {
            isTextSearch = true;
        }
        Logger.getLogger(App.class.getName()).log(Level.INFO, "> " + text);
        LocalDateTime startTime = LocalDateTime.parse((String) config.get(STIME), FORMATTER);
        String startTimeFormattedString = startTime.format(FORMATTER);
        Logger.getLogger(App.class.getName()).log(Level.INFO, "start time : " + startTimeFormattedString);
        LocalDateTime endTime = LocalDateTime.parse((String) config.get(ETIME), FORMATTER);
        String endTimeFormattedString = endTime.format(FORMATTER);
        Logger.getLogger(App.class.getName()).log(Level.INFO, "end time : " + endTimeFormattedString);
        int matchingIndex = getMatchingIndex(startTimeFormattedString, endTimeFormattedString);
        String subStartTimeFormattedString = startTimeFormattedString.substring(0, matchingIndex);
        Logger.getLogger(App.class.getName()).log(Level.INFO, "time comparison format : " + subStartTimeFormattedString);
        //String tillHourEndTimeFormattedString = endTimeFormattedString.substring(0, matchingIndex);
        //Logger.getLogger(App.class.getName()).log(Level.INFO, "end time comparison format : " + tillHourEndTimeFormattedString);
        String ctype = (String) config.get(CONNECTION);
        String outputFile = (String) config.get(RESULT_DIR) + (String) config.get(RESULT_NAME);
        if (C_LOCAL.equalsIgnoreCase(ctype)) {
            // local environment
            String originFile = (String) config.get(LOG_DIR) + (String) config.get(LOG_NAME);
            //String originFile = FILE_DIR + FILE_NAME;
            try (Stream<String> stream = Files.lines(Paths.get(originFile))) {
                try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(outputFile))) {
                    stream.filter(s -> {
                        if (!considerIt) {
                            if ( s.startsWith(subStartTimeFormattedString) ) {
                                considerIt = true;
                            }
                        }
                        if (isTextSearch) {
                            if ( s.contains(text) ) {
                                textMatch++;
                                textSearchMatching.append(s).append("\n");
                            }
                        }
                        return considerIt;
                    }).forEach(s -> filterAndWrite(s, bw, startTime, endTime));
                } catch (IOException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (C_SFTP.equalsIgnoreCase(ctype)) {
            // SFTP connection
            String originFile = (String) config.get(LOG_DIR) + (String) config.get(LOG_NAME);
            
            Session session = null;
            Channel channel = null;
            ChannelSftp channelSftp = null;
            
            String SFTPHOST = (String) config.get(SFTP_IP);
            int SFTPPORT = Integer.decode((String) config.get(SFTP_PORT) );
            String SFTPUSER = (String) config.get(SFTP_USER);
            String SFTPPASS = (String) config.get(SFTP_PASSWORD);
            try {
                JSch jsch = new JSch();
                
                session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
                session.setPassword(SFTPPASS);
                java.util.Properties pconfig = new java.util.Properties();
                pconfig.put("StrictHostKeyChecking", "no");
                session.setConfig(pconfig);
                session.connect();
                
                Logger.getLogger(App.class.getName()).log(Level.INFO, "Host connected!");
                
                channel = session.openChannel("sftp");
                channel.connect();
                
                Logger.getLogger(App.class.getName()).log(Level.INFO, "sftp channel opened and connected.");
                
                channelSftp = (ChannelSftp) channel;
                //Logger.getLogger(App.class.getName()).log(Level.INFO, " > " +channelSftp.pwd());
                //channelSftp.cd(text);
                //Logger.getLogger(App.class.getName()).log(Level.INFO, " > " +channelSftp.pwd());
                try (Stream<String> stream = new BufferedReader(new InputStreamReader(channelSftp.get(originFile))).lines() ) {
                    try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(outputFile))) {
                        stream.filter(s -> {
                            if (!considerIt) {
                                if ( s.startsWith(subStartTimeFormattedString) ) {
                                    considerIt = true;
                                }
                            }
                            if (isTextSearch) {
                                if ( s.contains(text) ) {
                                    textMatch++;
                                    textSearchMatching.append(s).append("\n");
                                }
                            }
                            return considerIt;
                        }).forEach(s -> filterAndWrite(s, bw, startTime, endTime));
                    } catch (IOException ex) {
                        Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } 
            } catch (Exception ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                channelSftp.exit();
                Logger.getLogger(App.class.getName()).log(Level.INFO, "sftp Channel exited.");
                channel.disconnect();
                Logger.getLogger(App.class.getName()).log(Level.INFO, "Channel disconnected.");
                session.disconnect();
                Logger.getLogger(App.class.getName()).log(Level.INFO, "Host Session disconnected.");
            }
        }
        if ( isTextSearch ) {
            Logger.getLogger(App.class.getName()).log(Level.INFO, "\n" + textSearchMatching.toString() + "Found " + textMatch + " item\n");
        }
    }

    private static int getMatchingIndex(String startTimeFormattedString, String endTimeFormattedString) {
        int matchingIndex = 0;
        int minLength = Math.min(startTimeFormattedString.length(), endTimeFormattedString.length());
        for ( int x = 0; x < minLength; x++ ) {
            if ( startTimeFormattedString.charAt(x) != endTimeFormattedString.charAt(x) ) {
                matchingIndex = x;
                break;
            }
        }
        return matchingIndex;
    }
    
    private static void filterAndWrite(String s, BufferedWriter bw, LocalDateTime startTime, LocalDateTime endTime) {
        Pattern p = Pattern.compile("\\d\\d\\d\\d.\\d\\d.\\d\\d \\d\\d:\\d\\d:\\d\\d.\\d\\d\\d");
        Matcher m = p.matcher(s);
        LocalDateTime cTime = null;
        if ( m.find() ) {
            cTime = LocalDateTime.parse(m.group(), FORMATTER);
        }
        if (cTime == null && takeIt) {
            try {
                //System.out.println(s);
                bw.write(s);
                bw.newLine();
            } catch (IOException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (cTime != null ) {
            if ( ( cTime.isAfter(startTime) || cTime.isEqual(startTime) ) && 
                    ( cTime.isBefore(endTime) || cTime.isEqual(endTime) ) ) {
                try {
                    takeIt = true;
                    //System.out.println(s);
                    bw.write(s);
                    bw.newLine();
                } catch (IOException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if ( cTime.isAfter(endTime) || cTime.isEqual(endTime) ) {
                considerIt = false;
            } else takeIt = false;
        }
    }
    
    private static JSONObject getConfigJsonObject() {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        try {

            Object obj = parser.parse(new FileReader(CTEMPCONFIGJSON));

            jsonObject = (JSONObject) obj;

        } catch (IOException | ParseException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonObject;
    }

    private static DateFormat getDateFormatFromString(String dateString) {
        for (Locale locale : DateFormat.getAvailableLocales()) {
            for (int style = DateFormat.FULL; style <= DateFormat.SHORT; style++) {
                DateFormat df = DateFormat.getDateInstance(style, locale);
                try {
                    df.parse(dateString);
                    return df;
                } catch ( java.text.ParseException e) {
                    continue;
                }
            }
        }
        return null;
    }
}
